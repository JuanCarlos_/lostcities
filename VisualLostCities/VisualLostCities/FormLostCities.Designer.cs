﻿namespace VisualLostCities
{
    partial class FormLostCities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile1Red = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile1Green = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile1Gray = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile1Blue = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile1Yellow = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile2Red = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile2Green = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile2Gray = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile2Blue = new System.Windows.Forms.PictureBox();
            this.pictureBoxPile2Yellow = new System.Windows.Forms.PictureBox();
            this.buttonColocar = new System.Windows.Forms.Button();
            this.buttonDescartar = new System.Windows.Forms.Button();
            this.labelNombreJ1 = new System.Windows.Forms.Label();
            this.buttonPile1 = new System.Windows.Forms.Button();
            this.buttonPile2 = new System.Windows.Forms.Button();
            this.buttonPile3 = new System.Windows.Forms.Button();
            this.buttonPile4 = new System.Windows.Forms.Button();
            this.buttonPile5 = new System.Windows.Forms.Button();
            this.buttonPile6 = new System.Windows.Forms.Button();
            this.buttonPile7 = new System.Windows.Forms.Button();
            this.buttonPile8 = new System.Windows.Forms.Button();
            this.buttonYellow = new System.Windows.Forms.Button();
            this.buttonBlue = new System.Windows.Forms.Button();
            this.buttonGray = new System.Windows.Forms.Button();
            this.buttonGreen = new System.Windows.Forms.Button();
            this.buttonRed = new System.Windows.Forms.Button();
            this.buttonMazo = new System.Windows.Forms.Button();
            this.labelCartasRestantes = new System.Windows.Forms.Label();
            this.labelNombreJ2 = new System.Windows.Forms.Label();
            this.labe1 = new System.Windows.Forms.Label();
            this.labelJugadorEnTurno = new System.Windows.Forms.Label();
            this.paneltrash = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Gray)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Yellow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Gray)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Yellow)).BeginInit();
            this.paneltrash.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::VisualLostCities.Properties.Resources.centro;
            this.pictureBox2.Location = new System.Drawing.Point(156, 193);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(950, 193);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBoxPile1Red
            // 
            this.pictureBoxPile1Red.Image = global::VisualLostCities.Properties.Resources.frojo;
            this.pictureBoxPile1Red.Location = new System.Drawing.Point(964, 416);
            this.pictureBoxPile1Red.Name = "pictureBoxPile1Red";
            this.pictureBoxPile1Red.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile1Red.TabIndex = 21;
            this.pictureBoxPile1Red.TabStop = false;
            // 
            // pictureBoxPile1Green
            // 
            this.pictureBoxPile1Green.Image = global::VisualLostCities.Properties.Resources.fverde;
            this.pictureBoxPile1Green.Location = new System.Drawing.Point(774, 416);
            this.pictureBoxPile1Green.Name = "pictureBoxPile1Green";
            this.pictureBoxPile1Green.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile1Green.TabIndex = 19;
            this.pictureBoxPile1Green.TabStop = false;
            // 
            // pictureBoxPile1Gray
            // 
            this.pictureBoxPile1Gray.Image = global::VisualLostCities.Properties.Resources.fgris;
            this.pictureBoxPile1Gray.Location = new System.Drawing.Point(582, 416);
            this.pictureBoxPile1Gray.Name = "pictureBoxPile1Gray";
            this.pictureBoxPile1Gray.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile1Gray.TabIndex = 18;
            this.pictureBoxPile1Gray.TabStop = false;
            // 
            // pictureBoxPile1Blue
            // 
            this.pictureBoxPile1Blue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPile1Blue.Image = global::VisualLostCities.Properties.Resources.fazul;
            this.pictureBoxPile1Blue.Location = new System.Drawing.Point(400, 416);
            this.pictureBoxPile1Blue.Name = "pictureBoxPile1Blue";
            this.pictureBoxPile1Blue.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile1Blue.TabIndex = 17;
            this.pictureBoxPile1Blue.TabStop = false;
            // 
            // pictureBoxPile1Yellow
            // 
            this.pictureBoxPile1Yellow.Image = global::VisualLostCities.Properties.Resources.famarillo;
            this.pictureBoxPile1Yellow.Location = new System.Drawing.Point(186, 416);
            this.pictureBoxPile1Yellow.Name = "pictureBoxPile1Yellow";
            this.pictureBoxPile1Yellow.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile1Yellow.TabIndex = 16;
            this.pictureBoxPile1Yellow.TabStop = false;
            // 
            // pictureBoxPile2Red
            // 
            this.pictureBoxPile2Red.Image = global::VisualLostCities.Properties.Resources.frojo;
            this.pictureBoxPile2Red.Location = new System.Drawing.Point(964, 63);
            this.pictureBoxPile2Red.Name = "pictureBoxPile2Red";
            this.pictureBoxPile2Red.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile2Red.TabIndex = 26;
            this.pictureBoxPile2Red.TabStop = false;
            // 
            // pictureBoxPile2Green
            // 
            this.pictureBoxPile2Green.Image = global::VisualLostCities.Properties.Resources.fverde;
            this.pictureBoxPile2Green.Location = new System.Drawing.Point(774, 63);
            this.pictureBoxPile2Green.Name = "pictureBoxPile2Green";
            this.pictureBoxPile2Green.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile2Green.TabIndex = 25;
            this.pictureBoxPile2Green.TabStop = false;
            // 
            // pictureBoxPile2Gray
            // 
            this.pictureBoxPile2Gray.Image = global::VisualLostCities.Properties.Resources.fgris;
            this.pictureBoxPile2Gray.Location = new System.Drawing.Point(582, 63);
            this.pictureBoxPile2Gray.Name = "pictureBoxPile2Gray";
            this.pictureBoxPile2Gray.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile2Gray.TabIndex = 24;
            this.pictureBoxPile2Gray.TabStop = false;
            // 
            // pictureBoxPile2Blue
            // 
            this.pictureBoxPile2Blue.Image = global::VisualLostCities.Properties.Resources.fazul;
            this.pictureBoxPile2Blue.Location = new System.Drawing.Point(400, 63);
            this.pictureBoxPile2Blue.Name = "pictureBoxPile2Blue";
            this.pictureBoxPile2Blue.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile2Blue.TabIndex = 23;
            this.pictureBoxPile2Blue.TabStop = false;
            // 
            // pictureBoxPile2Yellow
            // 
            this.pictureBoxPile2Yellow.Image = global::VisualLostCities.Properties.Resources.famarillo;
            this.pictureBoxPile2Yellow.Location = new System.Drawing.Point(186, 63);
            this.pictureBoxPile2Yellow.Name = "pictureBoxPile2Yellow";
            this.pictureBoxPile2Yellow.Size = new System.Drawing.Size(132, 99);
            this.pictureBoxPile2Yellow.TabIndex = 22;
            this.pictureBoxPile2Yellow.TabStop = false;
            // 
            // buttonColocar
            // 
            this.buttonColocar.Location = new System.Drawing.Point(1163, 492);
            this.buttonColocar.Name = "buttonColocar";
            this.buttonColocar.Size = new System.Drawing.Size(75, 23);
            this.buttonColocar.TabIndex = 27;
            this.buttonColocar.Text = "Colocar";
            this.buttonColocar.UseVisualStyleBackColor = true;
            this.buttonColocar.Click += new System.EventHandler(this.buttonColocar_Click);
            // 
            // buttonDescartar
            // 
            this.buttonDescartar.Location = new System.Drawing.Point(1163, 534);
            this.buttonDescartar.Name = "buttonDescartar";
            this.buttonDescartar.Size = new System.Drawing.Size(75, 23);
            this.buttonDescartar.TabIndex = 28;
            this.buttonDescartar.Text = "Descartar";
            this.buttonDescartar.UseVisualStyleBackColor = true;
            this.buttonDescartar.Click += new System.EventHandler(this.buttonDescartar_Click);
            // 
            // labelNombreJ1
            // 
            this.labelNombreJ1.AutoSize = true;
            this.labelNombreJ1.BackColor = System.Drawing.Color.Transparent;
            this.labelNombreJ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNombreJ1.ForeColor = System.Drawing.Color.White;
            this.labelNombreJ1.Location = new System.Drawing.Point(12, 439);
            this.labelNombreJ1.Name = "labelNombreJ1";
            this.labelNombreJ1.Size = new System.Drawing.Size(98, 24);
            this.labelNombreJ1.TabIndex = 30;
            this.labelNombreJ1.Text = "Jugador: ";
            // 
            // buttonPile1
            // 
            this.buttonPile1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPile1.Location = new System.Drawing.Point(143, 556);
            this.buttonPile1.Name = "buttonPile1";
            this.buttonPile1.Size = new System.Drawing.Size(70, 105);
            this.buttonPile1.TabIndex = 31;
            this.buttonPile1.UseVisualStyleBackColor = true;
            this.buttonPile1.Click += new System.EventHandler(this.buttonPile1_Click);
            // 
            // buttonPile2
            // 
            this.buttonPile2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPile2.Location = new System.Drawing.Point(273, 556);
            this.buttonPile2.Name = "buttonPile2";
            this.buttonPile2.Size = new System.Drawing.Size(70, 105);
            this.buttonPile2.TabIndex = 32;
            this.buttonPile2.UseVisualStyleBackColor = true;
            this.buttonPile2.Click += new System.EventHandler(this.buttonPile2_Click);
            // 
            // buttonPile3
            // 
            this.buttonPile3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPile3.Location = new System.Drawing.Point(403, 556);
            this.buttonPile3.Name = "buttonPile3";
            this.buttonPile3.Size = new System.Drawing.Size(70, 105);
            this.buttonPile3.TabIndex = 33;
            this.buttonPile3.UseVisualStyleBackColor = true;
            this.buttonPile3.Click += new System.EventHandler(this.buttonPile3_Click);
            // 
            // buttonPile4
            // 
            this.buttonPile4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPile4.Location = new System.Drawing.Point(533, 556);
            this.buttonPile4.Name = "buttonPile4";
            this.buttonPile4.Size = new System.Drawing.Size(70, 105);
            this.buttonPile4.TabIndex = 34;
            this.buttonPile4.UseVisualStyleBackColor = true;
            this.buttonPile4.Click += new System.EventHandler(this.buttonPile4_Click);
            // 
            // buttonPile5
            // 
            this.buttonPile5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPile5.Location = new System.Drawing.Point(663, 556);
            this.buttonPile5.Name = "buttonPile5";
            this.buttonPile5.Size = new System.Drawing.Size(70, 105);
            this.buttonPile5.TabIndex = 35;
            this.buttonPile5.UseVisualStyleBackColor = true;
            this.buttonPile5.Click += new System.EventHandler(this.buttonPile5_Click);
            // 
            // buttonPile6
            // 
            this.buttonPile6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPile6.Location = new System.Drawing.Point(793, 556);
            this.buttonPile6.Name = "buttonPile6";
            this.buttonPile6.Size = new System.Drawing.Size(70, 105);
            this.buttonPile6.TabIndex = 36;
            this.buttonPile6.UseVisualStyleBackColor = true;
            this.buttonPile6.Click += new System.EventHandler(this.buttonPile6_Click);
            // 
            // buttonPile7
            // 
            this.buttonPile7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPile7.Location = new System.Drawing.Point(923, 556);
            this.buttonPile7.Name = "buttonPile7";
            this.buttonPile7.Size = new System.Drawing.Size(70, 105);
            this.buttonPile7.TabIndex = 37;
            this.buttonPile7.UseVisualStyleBackColor = true;
            this.buttonPile7.Click += new System.EventHandler(this.buttonPile7_Click);
            // 
            // buttonPile8
            // 
            this.buttonPile8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPile8.Location = new System.Drawing.Point(1053, 556);
            this.buttonPile8.Name = "buttonPile8";
            this.buttonPile8.Size = new System.Drawing.Size(70, 105);
            this.buttonPile8.TabIndex = 38;
            this.buttonPile8.UseVisualStyleBackColor = true;
            this.buttonPile8.Click += new System.EventHandler(this.buttonPile8_Click);
            // 
            // buttonYellow
            // 
            this.buttonYellow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonYellow.Image = global::VisualLostCities.Properties.Resources.yellow;
            this.buttonYellow.Location = new System.Drawing.Point(236, 242);
            this.buttonYellow.Name = "buttonYellow";
            this.buttonYellow.Size = new System.Drawing.Size(70, 105);
            this.buttonYellow.TabIndex = 39;
            this.buttonYellow.UseVisualStyleBackColor = true;
            this.buttonYellow.Click += new System.EventHandler(this.buttonYellow_Click);
            // 
            // buttonBlue
            // 
            this.buttonBlue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBlue.Image = global::VisualLostCities.Properties.Resources.blue;
            this.buttonBlue.Location = new System.Drawing.Point(403, 242);
            this.buttonBlue.Name = "buttonBlue";
            this.buttonBlue.Size = new System.Drawing.Size(70, 105);
            this.buttonBlue.TabIndex = 40;
            this.buttonBlue.UseVisualStyleBackColor = true;
            this.buttonBlue.Click += new System.EventHandler(this.buttonBlue_Click);
            // 
            // buttonGray
            // 
            this.buttonGray.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGray.Image = global::VisualLostCities.Properties.Resources.gray;
            this.buttonGray.Location = new System.Drawing.Point(582, 242);
            this.buttonGray.Name = "buttonGray";
            this.buttonGray.Size = new System.Drawing.Size(70, 105);
            this.buttonGray.TabIndex = 41;
            this.buttonGray.UseVisualStyleBackColor = true;
            this.buttonGray.Click += new System.EventHandler(this.buttonGray_Click);
            // 
            // buttonGreen
            // 
            this.buttonGreen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGreen.Image = global::VisualLostCities.Properties.Resources.green;
            this.buttonGreen.Location = new System.Drawing.Point(774, 242);
            this.buttonGreen.Name = "buttonGreen";
            this.buttonGreen.Size = new System.Drawing.Size(70, 105);
            this.buttonGreen.TabIndex = 42;
            this.buttonGreen.UseVisualStyleBackColor = true;
            this.buttonGreen.Click += new System.EventHandler(this.buttonGreen_Click);
            // 
            // buttonRed
            // 
            this.buttonRed.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonRed.Image = global::VisualLostCities.Properties.Resources.red;
            this.buttonRed.Location = new System.Drawing.Point(964, 242);
            this.buttonRed.Name = "buttonRed";
            this.buttonRed.Size = new System.Drawing.Size(70, 105);
            this.buttonRed.TabIndex = 43;
            this.buttonRed.UseVisualStyleBackColor = true;
            this.buttonRed.Click += new System.EventHandler(this.buttonRed_Click);
            // 
            // buttonMazo
            // 
            this.buttonMazo.Image = global::VisualLostCities.Properties.Resources.deckbackside;
            this.buttonMazo.Location = new System.Drawing.Point(1122, 262);
            this.buttonMazo.Name = "buttonMazo";
            this.buttonMazo.Size = new System.Drawing.Size(116, 174);
            this.buttonMazo.TabIndex = 44;
            this.buttonMazo.UseVisualStyleBackColor = true;
            this.buttonMazo.Click += new System.EventHandler(this.buttonMazo_Click);
            // 
            // labelCartasRestantes
            // 
            this.labelCartasRestantes.AutoSize = true;
            this.labelCartasRestantes.BackColor = System.Drawing.Color.Transparent;
            this.labelCartasRestantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCartasRestantes.ForeColor = System.Drawing.Color.White;
            this.labelCartasRestantes.Location = new System.Drawing.Point(1122, 223);
            this.labelCartasRestantes.Name = "labelCartasRestantes";
            this.labelCartasRestantes.Size = new System.Drawing.Size(153, 20);
            this.labelCartasRestantes.TabIndex = 45;
            this.labelCartasRestantes.Text = "Cartas restantes: ";
            // 
            // labelNombreJ2
            // 
            this.labelNombreJ2.AutoSize = true;
            this.labelNombreJ2.BackColor = System.Drawing.Color.Transparent;
            this.labelNombreJ2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNombreJ2.ForeColor = System.Drawing.Color.White;
            this.labelNombreJ2.Location = new System.Drawing.Point(12, 75);
            this.labelNombreJ2.Name = "labelNombreJ2";
            this.labelNombreJ2.Size = new System.Drawing.Size(98, 24);
            this.labelNombreJ2.TabIndex = 46;
            this.labelNombreJ2.Text = "Jugador: ";
            // 
            // labe1
            // 
            this.labe1.AutoSize = true;
            this.labe1.BackColor = System.Drawing.Color.Transparent;
            this.labe1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labe1.ForeColor = System.Drawing.Color.White;
            this.labe1.Location = new System.Drawing.Point(-3, 578);
            this.labe1.Name = "labe1";
            this.labe1.Size = new System.Drawing.Size(90, 24);
            this.labe1.TabIndex = 47;
            this.labe1.Text = "En turno";
            // 
            // labelJugadorEnTurno
            // 
            this.labelJugadorEnTurno.AutoSize = true;
            this.labelJugadorEnTurno.BackColor = System.Drawing.Color.Transparent;
            this.labelJugadorEnTurno.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJugadorEnTurno.ForeColor = System.Drawing.Color.White;
            this.labelJugadorEnTurno.Location = new System.Drawing.Point(12, 614);
            this.labelJugadorEnTurno.Name = "labelJugadorEnTurno";
            this.labelJugadorEnTurno.Size = new System.Drawing.Size(98, 24);
            this.labelJugadorEnTurno.TabIndex = 48;
            this.labelJugadorEnTurno.Text = "Jugador: ";
            // 
            // paneltrash
            // 
            this.paneltrash.BackgroundImage = global::VisualLostCities.Properties.Resources.back;
            this.paneltrash.Controls.Add(this.label1);
            this.paneltrash.Controls.Add(this.button1);
            this.paneltrash.Location = new System.Drawing.Point(145, 556);
            this.paneltrash.Name = "paneltrash";
            this.paneltrash.Size = new System.Drawing.Size(1000, 107);
            this.paneltrash.TabIndex = 50;
            this.paneltrash.Visible = false;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::VisualLostCities.Properties.Resources.centro;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(381, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(508, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cambio de jugador";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 23F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(5, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(506, 35);
            this.label1.TabIndex = 1;
            this.label1.Text = "¿Ya estas listo? da click en el boton";
            // 
            // FormLostCities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::VisualLostCities.Properties.Resources.back;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1250, 673);
            this.Controls.Add(this.paneltrash);
            this.Controls.Add(this.labelJugadorEnTurno);
            this.Controls.Add(this.labe1);
            this.Controls.Add(this.labelNombreJ2);
            this.Controls.Add(this.labelCartasRestantes);
            this.Controls.Add(this.buttonMazo);
            this.Controls.Add(this.buttonRed);
            this.Controls.Add(this.buttonGreen);
            this.Controls.Add(this.buttonGray);
            this.Controls.Add(this.buttonBlue);
            this.Controls.Add(this.buttonYellow);
            this.Controls.Add(this.buttonPile8);
            this.Controls.Add(this.buttonPile7);
            this.Controls.Add(this.buttonPile6);
            this.Controls.Add(this.buttonPile5);
            this.Controls.Add(this.buttonPile4);
            this.Controls.Add(this.buttonPile3);
            this.Controls.Add(this.buttonPile2);
            this.Controls.Add(this.buttonPile1);
            this.Controls.Add(this.labelNombreJ1);
            this.Controls.Add(this.buttonDescartar);
            this.Controls.Add(this.buttonColocar);
            this.Controls.Add(this.pictureBoxPile2Red);
            this.Controls.Add(this.pictureBoxPile2Green);
            this.Controls.Add(this.pictureBoxPile2Gray);
            this.Controls.Add(this.pictureBoxPile2Blue);
            this.Controls.Add(this.pictureBoxPile2Yellow);
            this.Controls.Add(this.pictureBoxPile1Red);
            this.Controls.Add(this.pictureBoxPile1Green);
            this.Controls.Add(this.pictureBoxPile1Gray);
            this.Controls.Add(this.pictureBoxPile1Blue);
            this.Controls.Add(this.pictureBoxPile1Yellow);
            this.Controls.Add(this.pictureBox2);
            this.Name = "FormLostCities";
            this.Text = "LostCities by ISC\'s 6to semestre";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormLostCities_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Gray)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile1Yellow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Gray)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPile2Yellow)).EndInit();
            this.paneltrash.ResumeLayout(false);
            this.paneltrash.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBoxPile1Red;
        private System.Windows.Forms.PictureBox pictureBoxPile1Green;
        private System.Windows.Forms.PictureBox pictureBoxPile1Gray;
        private System.Windows.Forms.PictureBox pictureBoxPile1Blue;
        private System.Windows.Forms.PictureBox pictureBoxPile1Yellow;
        private System.Windows.Forms.PictureBox pictureBoxPile2Red;
        private System.Windows.Forms.PictureBox pictureBoxPile2Green;
        private System.Windows.Forms.PictureBox pictureBoxPile2Gray;
        private System.Windows.Forms.PictureBox pictureBoxPile2Blue;
        private System.Windows.Forms.PictureBox pictureBoxPile2Yellow;
        private System.Windows.Forms.Button buttonColocar;
        private System.Windows.Forms.Button buttonDescartar;
        private System.Windows.Forms.Label labelNombreJ1;
        private System.Windows.Forms.Button buttonPile1;
        private System.Windows.Forms.Button buttonPile2;
        private System.Windows.Forms.Button buttonPile3;
        private System.Windows.Forms.Button buttonPile4;
        private System.Windows.Forms.Button buttonPile5;
        private System.Windows.Forms.Button buttonPile6;
        private System.Windows.Forms.Button buttonPile7;
        private System.Windows.Forms.Button buttonPile8;
        private System.Windows.Forms.Button buttonYellow;
        private System.Windows.Forms.Button buttonBlue;
        private System.Windows.Forms.Button buttonGray;
        private System.Windows.Forms.Button buttonGreen;
        private System.Windows.Forms.Button buttonRed;
        private System.Windows.Forms.Button buttonMazo;
        private System.Windows.Forms.Label labelCartasRestantes;
        private System.Windows.Forms.Label labelNombreJ2;
        private System.Windows.Forms.Label labe1;
        private System.Windows.Forms.Label labelJugadorEnTurno;
        private System.Windows.Forms.Panel paneltrash;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}