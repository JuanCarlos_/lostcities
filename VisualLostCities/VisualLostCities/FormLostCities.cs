﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LostCities;


namespace VisualLostCities
{
    public partial class FormLostCities : Form
    {
        string[] names;
        int[] ages;
        Game game;
        int index = -1;
        int currentplayer;
        

        public FormLostCities(string[] names, int[] ages)
        {
            InitializeComponent();
            this.ages = ages;
            this.names = names;
            currentplayer = 1;
            game = new Game();

            if (ages[0] >= ages[1])
            {
                game.addPlayer(names[0]);
                game.addPlayer(names[1]);
            }
            else
            {
                game.addPlayer(names[1]);
                game.addPlayer(names[0]);
            }
            game.setHand(game.player1);
            game.setHand(game.player2);
            labelCartasRestantes.Text = "Cartas restantes: " + game.getLeftCards();
        }

        private void FormLostCities_Load(object sender, EventArgs e)
        {
            labelNombreJ1.Text = "Jugador: " + game.player1.getName();
            labelNombreJ2.Text = "Jugador: " + game.player2.getName();
            labelJugadorEnTurno.Text = game.player1.getName();
            updateHand(currentplayer);
            buttonMazo.Enabled = false;
            if (index == -1) 
            {
                disableButtons();
            }
        }

        private void updateHand(int player)
        {
            if (player == 1)
            {
                buttonPile1.BackgroundImage = Image.FromFile("../../img/" + game.player1.getHand()[0].getColor().ToString().ToLower() + game.player1.getHand()[0].getId().ToString() + ".png");
                buttonPile2.BackgroundImage = Image.FromFile("../../img/" + game.player1.getHand()[1].getColor().ToString().ToLower() + game.player1.getHand()[1].getId().ToString() + ".png");
                buttonPile3.BackgroundImage = Image.FromFile("../../img/" + game.player1.getHand()[2].getColor().ToString().ToLower() + game.player1.getHand()[2].getId().ToString() + ".png");
                buttonPile4.BackgroundImage = Image.FromFile("../../img/" + game.player1.getHand()[3].getColor().ToString().ToLower() + game.player1.getHand()[3].getId().ToString() + ".png");
                buttonPile5.BackgroundImage = Image.FromFile("../../img/" + game.player1.getHand()[4].getColor().ToString().ToLower() + game.player1.getHand()[4].getId().ToString() + ".png");
                buttonPile6.BackgroundImage = Image.FromFile("../../img/" + game.player1.getHand()[5].getColor().ToString().ToLower() + game.player1.getHand()[5].getId().ToString() + ".png");
                buttonPile7.BackgroundImage = Image.FromFile("../../img/" + game.player1.getHand()[6].getColor().ToString().ToLower() + game.player1.getHand()[6].getId().ToString() + ".png");
                buttonPile8.BackgroundImage = Image.FromFile("../../img/" + game.player1.getHand()[7].getColor().ToString().ToLower() + game.player1.getHand()[7].getId().ToString() + ".png");
            }
            else
            {
                buttonPile1.BackgroundImage = Image.FromFile("../../img/" + game.player2.getHand()[0].getColor().ToString().ToLower() + game.player2.getHand()[0].getId().ToString() + ".png");
                buttonPile2.BackgroundImage = Image.FromFile("../../img/" + game.player2.getHand()[1].getColor().ToString().ToLower() + game.player2.getHand()[1].getId().ToString() + ".png");
                buttonPile3.BackgroundImage = Image.FromFile("../../img/" + game.player2.getHand()[2].getColor().ToString().ToLower() + game.player2.getHand()[2].getId().ToString() + ".png");
                buttonPile4.BackgroundImage = Image.FromFile("../../img/" + game.player2.getHand()[3].getColor().ToString().ToLower() + game.player2.getHand()[3].getId().ToString() + ".png");
                buttonPile5.BackgroundImage = Image.FromFile("../../img/" + game.player2.getHand()[4].getColor().ToString().ToLower() + game.player2.getHand()[4].getId().ToString() + ".png");
                buttonPile6.BackgroundImage = Image.FromFile("../../img/" + game.player2.getHand()[5].getColor().ToString().ToLower() + game.player2.getHand()[5].getId().ToString() + ".png");
                buttonPile7.BackgroundImage = Image.FromFile("../../img/" + game.player2.getHand()[6].getColor().ToString().ToLower() + game.player2.getHand()[6].getId().ToString() + ".png");
                buttonPile8.BackgroundImage = Image.FromFile("../../img/" + game.player2.getHand()[7].getColor().ToString().ToLower() + game.player2.getHand()[7].getId().ToString() + ".png");
            }
        }

        private void buttonColocar_Click(object sender, EventArgs e)
        {
            if(currentplayer == 1){
                if (game.setCard(game.player1, new Card(game.player1.getHand()[index].getId(), game.player1.getHand()[index].getColor())))
                {
                    disableHand();
                    enableDiscarded();
                    paintThumb(game.player1.getHand()[index].getId(), game.player1.getHand()[index].getColor());
                    game.player1.getHand().RemoveAt(index);
                }
                else
                {
                    MessageBox.Show("No se puede colocar, ya hay un numero mayor");
                }
            }
            else
            {
                if (game.setCard(game.player2, new Card(game.player2.getHand()[index].getId(), game.player2.getHand()[index].getColor())))
                {
                    disableHand();
                    enableDiscarded();
                    paintThumb(game.player2.getHand()[index].getId(), game.player2.getHand()[index].getColor());
                    game.player2.getHand().RemoveAt(index);
                }
                else
                {
                    MessageBox.Show("No se puede colocar, ya hay un numero mayor");
                }
            }
            disableButtons();
        }

        private int getX(int total) 
        {
            if (total<=4){
                return total-1;
            }
            else {
                if (total <= 8)
                {
                    return total - 5;
                }
                else 
                {
                    return total - 9;
                }
            } 
        }

        private int getY(int total) 
        {
            if (total <= 4) {
                return 0;
            }
            else if(total<=8)  {
            return 1;
            }
            return 2;
        }

        private void paintThumb(int id, Colors color)
        {
            Image image;
            int x=0, y=0;
            switch (color)
            {
                case Colors.Blue:
                    {
                        if (currentplayer == 1)
                        {
                            image = (Image)pictureBoxPile1Blue.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.bluepile1.Count) * 33;
                            y = getY(game.board.bluepile1.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile1Blue.Image = image;
                        }
                        else
                        {
                            image = (Image)pictureBoxPile2Blue.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.bluepile2.Count) * 33;
                            y = getY(game.board.bluepile2.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile2Blue.Image = image;
                        }
                        break;
                    }

                case Colors.Gray:
                    {
                        if (currentplayer == 1)
                        {
                            image = (Image)pictureBoxPile1Gray.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.graypile1.Count) * 33;
                            y = getY(game.board.graypile1.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile1Gray.Image = image;
                        }
                        else
                        {
                            image = (Image)pictureBoxPile2Gray.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.graypile2.Count) * 33;
                            y = getY(game.board.graypile2.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile2Gray.Image = image;
                        }
                        break;
                    }

                case Colors.Green:
                    {
                        if (currentplayer == 1)
                        {
                            image = (Image)pictureBoxPile1Green.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.greenpile1.Count) * 33;
                            y = getY(game.board.greenpile1.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile1Green.Image = image;
                        }
                        else
                        {
                            image = (Image)pictureBoxPile2Green.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.greenpile2.Count) * 33;
                            y = getY(game.board.greenpile2.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile2Green.Image = image;
                        }
                        break;
                    }

                case Colors.Red:
                    {
                        if (currentplayer == 1)
                        {
                            image = (Image)pictureBoxPile1Red.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.redpile1.Count) * 33;
                            y = getY(game.board.redpile1.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile1Red.Image = image;
                        }
                        else
                        {
                            image = (Image)pictureBoxPile2Red.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.redpile2.Count) * 33;
                            y = getY(game.board.redpile2.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile2Red.Image = image;
                        }
                        break;
                    }

                case Colors.Yellow:
                    {
                        if (currentplayer == 1)
                        {
                            image = (Image)pictureBoxPile1Yellow.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.yellowpile1.Count) * 33;
                            y = getY(game.board.yellowpile1.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile1Yellow.Image = image;
                        }
                        else
                        {
                            image = (Image)pictureBoxPile2Yellow.Image.Clone();
                            Graphics g = Graphics.FromImage(image);
                            x = getX(game.board.yellowpile2.Count) * 33;
                            y = getY(game.board.yellowpile2.Count) * 33;
                            g.DrawImage(Image.FromFile("../../img/m" + color + id + ".png"), new Point(x, y));
                            pictureBoxPile2Yellow.Image = image;
                        }
                        break;
                    }
            }
            
        }

        private void buttonPile1_Click(object sender, EventArgs e)
        {
            index = 0;
            enableButtons();
        }

        private void buttonPile2_Click(object sender, EventArgs e)
        {
            index = 1;
            enableButtons();
        }

        private void buttonPile3_Click(object sender, EventArgs e)
        {
            index = 2;
            enableButtons();
        }

        private void buttonPile4_Click(object sender, EventArgs e)
        {
            index = 3;
            enableButtons();
        }

        private void buttonPile5_Click(object sender, EventArgs e)
        {
            index = 4;
            enableButtons();
        }

        private void buttonPile6_Click(object sender, EventArgs e)
        {
            index = 5;
            enableButtons();
        }

        private void buttonPile7_Click(object sender, EventArgs e)
        {
            index = 6;
            enableButtons();
        }

        private void buttonPile8_Click(object sender, EventArgs e)
        {
            index = 7;
            enableButtons();
        }

        private void buttonDescartar_Click(object sender, EventArgs e)
        {
            if (currentplayer == 1)
            {
                switch (game.player1.getHand()[index].getColor())
                {
                    case Colors.Blue:
                        buttonBlue.Image = Image.FromFile("../../img/" + game.player1.getHand()[index].getColor().ToString().ToLower() + game.player1.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                    case Colors.Gray:
                        buttonGray.Image = Image.FromFile("../../img/" + game.player1.getHand()[index].getColor().ToString().ToLower() + game.player1.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                    case Colors.Green:
                        buttonGreen.Image = Image.FromFile("../../img/" + game.player1.getHand()[index].getColor().ToString().ToLower() + game.player1.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                    case Colors.Red:
                        buttonRed.Image = Image.FromFile("../../img/" + game.player1.getHand()[index].getColor().ToString().ToLower() + game.player1.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                    case Colors.Yellow:
                        buttonYellow.Image = Image.FromFile("../../img/" + game.player1.getHand()[index].getColor().ToString().ToLower() + game.player1.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                }
                game.discardCard(game.player1, index);
            }
            else
            {
                switch (game.player2.getHand()[index].getColor())
                {
                    case Colors.Blue:
                        buttonBlue.Image = Image.FromFile("../../img/" + game.player2.getHand()[index].getColor().ToString().ToLower() + game.player2.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                    case Colors.Gray:
                        buttonGray.Image = Image.FromFile("../../img/" + game.player2.getHand()[index].getColor().ToString().ToLower() + game.player2.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                    case Colors.Green:
                        buttonGreen.Image = Image.FromFile("../../img/" + game.player2.getHand()[index].getColor().ToString().ToLower() + game.player2.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                    case Colors.Red:
                        buttonRed.Image = Image.FromFile("../../img/" + game.player2.getHand()[index].getColor().ToString().ToLower() + game.player2.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                    case Colors.Yellow:
                        buttonYellow.Image = Image.FromFile("../../img/" + game.player2.getHand()[index].getColor().ToString().ToLower() + game.player2.getHand()[index].getId().ToString().ToLower() + ".png");
                        break;
                }
                game.discardCard(game.player2, index);
            }

            enableDiscarded();
            disableHand();
            disableButtons();
        }

        private void disableHand()
        {
            buttonPile1.Enabled = false;
            buttonPile2.Enabled = false;
            buttonPile3.Enabled = false;
            buttonPile4.Enabled = false;
            buttonPile5.Enabled = false;
            buttonPile6.Enabled = false;
            buttonPile7.Enabled = false;
            buttonPile8.Enabled = false;
        }

        private void enableHand()
        {
            buttonPile1.Enabled = true;
            buttonPile2.Enabled = true;
            buttonPile3.Enabled = true;
            buttonPile4.Enabled = true;
            buttonPile5.Enabled = true;
            buttonPile6.Enabled = true;
            buttonPile7.Enabled = true;
            buttonPile8.Enabled = true;
        }

        private void enableDiscarded(){
            buttonBlue.Enabled = true;
            buttonYellow.Enabled = true;
            buttonRed.Enabled = true;
            buttonGreen.Enabled = true;
            buttonGray.Enabled = true;
            buttonMazo.Enabled = true;
        }

        private void disableDiscarded()
        {
            buttonBlue.Enabled = false;
            buttonYellow.Enabled = false;
            buttonRed.Enabled = false;
            buttonGreen.Enabled = false;
            buttonGray.Enabled = false;
            buttonMazo.Enabled = false;
        }

        private void enableButtons() 
        {
            buttonDescartar.Enabled = true;
            buttonColocar.Enabled = true;  
        }

        private void disableButtons() 
        {
            buttonDescartar.Enabled = false;
            buttonColocar.Enabled = false;
        }

        private void buttonYellow_Click(object sender, EventArgs e)
        {
            if (game.board.discardedyellowpile != null && game.board.discardedyellowpile.Count > 0)
            {
                if (currentplayer == 1)
                {
                    game.addCard(game.player1, game.board.discardedyellowpile.Pop());
                }
                else
                {
                    game.addCard(game.player2, game.board.discardedyellowpile.Pop());
                }
                    updateHand(currentplayer);
                    disableDiscarded();
                    enableHand();
                if (currentplayer == 1)
                {
                    currentplayer = 2;
                }
                else
                {
                    currentplayer = 1;
                }
                nextPlayer();
                paneltrash.Visible = true;
                
                if (game.board.discardedyellowpile.Count > 0)
                {
                    buttonYellow.Image = Image.FromFile("../../img/" + game.board.discardedyellowpile.Peek().getColor() + game.board.discardedyellowpile.Peek().getId() + ".png");
                }
                else
                {
                    buttonYellow.Image = Image.FromFile("../../Resources/yellow.png");
                }
            }
                       
        }

        private void buttonBlue_Click(object sender, EventArgs e)
        {
            if (game.board.discardedbluepile != null && game.board.discardedbluepile.Count > 0)
            {
                if (currentplayer == 1)
                {
                    game.addCard(game.player1, game.board.discardedbluepile.Pop());
                }
                else
                {
                    game.addCard(game.player2, game.board.discardedbluepile.Pop());
                }
                updateHand(currentplayer);
                disableDiscarded();
                enableHand();
                if (currentplayer == 1)
                {
                    currentplayer = 2;
                }
                else
                {
                    currentplayer = 1;
                }
                nextPlayer();
                paneltrash.Visible = true;

                if (game.board.discardedbluepile.Count > 0)
                {
                    buttonBlue.Image = Image.FromFile("../../img/" + game.board.discardedbluepile.Peek().getColor() + game.board.discardedbluepile.Peek().getId() + ".png");
                }
                else
                {
                    buttonBlue.Image = Image.FromFile("../../Resources/blue.png");
                }
            }
        }

        private void buttonGray_Click(object sender, EventArgs e)
        {
            if (game.board.discardedgraypile != null && game.board.discardedgraypile.Count > 0)
            {
                if (currentplayer == 1)
                {
                    game.addCard(game.player1, game.board.discardedgraypile.Pop());
                }
                else
                {
                    game.addCard(game.player2, game.board.discardedgraypile.Pop());
                }
                updateHand(currentplayer);
                disableDiscarded();
                enableHand();
                if (currentplayer == 1)
                {
                    currentplayer = 2;
                }
                else
                {
                    currentplayer = 1;
                }
                nextPlayer();
                paneltrash.Visible = true;

                if (game.board.discardedgraypile.Count > 0)
                {
                    buttonGray.Image = Image.FromFile("../../img/" + game.board.discardedgraypile.Peek().getColor() + game.board.discardedgraypile.Peek().getId() + ".png");
                }
                else
                {
                  
                    buttonGray.Image = Image.FromFile("../../Resources/gray.png");
                }
            }
        }

        private void buttonGreen_Click(object sender, EventArgs e)
        {
            if (game.board.discardedgreenpile != null && game.board.discardedgreenpile.Count > 0)
            {
                if (currentplayer == 1)
                {
                    game.addCard(game.player1, game.board.discardedgreenpile.Pop());
                }
                else
                {
                    game.addCard(game.player2, game.board.discardedgreenpile.Pop());
                }
                updateHand(currentplayer);
                disableDiscarded();
                enableHand();
                if (currentplayer == 1)
                {
                    currentplayer = 2;
                }
                else
                {
                    currentplayer = 1;
                }
                nextPlayer();
                paneltrash.Visible = true;

                if (game.board.discardedgreenpile.Count > 0)
                {
                    buttonGreen.Image = Image.FromFile("../../img/" + game.board.discardedgreenpile.Peek().getColor() + game.board.discardedgreenpile.Peek().getId() + ".png");
                }
                else
                {
                    buttonGreen.Image = Image.FromFile("../../Resources/green.png");
                }
            }
        }

        private void buttonRed_Click(object sender, EventArgs e)
        {
            if (game.board.discardedredpile != null && game.board.discardedredpile.Count > 0)
            {
                if (currentplayer == 1)
                {
                    game.addCard(game.player1, game.board.discardedredpile.Pop());
                }
                else
                {
                    game.addCard(game.player2, game.board.discardedredpile.Pop());
                }
                updateHand(currentplayer);
                disableDiscarded();
                enableHand();
                if (currentplayer == 1)
                {
                    currentplayer = 2;
                }
                else
                {
                    currentplayer = 1;
                }
                nextPlayer();
                paneltrash.Visible = true;
                if (game.board.discardedredpile.Count > 0)
                {
                    buttonRed.Image = Image.FromFile("../../img/" + game.board.discardedredpile.Peek().getColor() + game.board.discardedredpile.Peek().getId() + ".png");
                }
                else
                {
                    buttonRed.Image = Image.FromFile("../../Resources/red.png");
                }
            }
        }

        private void buttonMazo_Click(object sender, EventArgs e)
        {
            if (game.board.deck.hasCards())
            {
                if (currentplayer == 1)
                {
                    game.addCard(game.player1, game.nextCard());
                    disableDiscarded();
                    enableHand();
                    updateHand(currentplayer);
                    currentplayer = 2;
                    nextPlayer();
                    paneltrash.Visible = true;
                }
                else
                {
                    game.addCard(game.player2, game.nextCard());
                    disableDiscarded();
                    enableHand();
                    updateHand(currentplayer);
                    currentplayer = 1;
                    nextPlayer();
                    paneltrash.Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Se acabo el juego");
                string[] names = new string[2];
                names[0] = game.player1.getName();
                names[1] = game.player2.getName();
                GameOver gameover = new GameOver(calculatePoints(), names);
                gameover.Show();
            }
            labelCartasRestantes.Text = "Cartas restantes: " + game.getLeftCards();
        }

        private void nextPlayer()
        {
            updateHand(currentplayer);
            if (currentplayer == 1)
            {
                labelJugadorEnTurno.Text = game.player1.getName();
            }
            else
            {
                labelJugadorEnTurno.Text = game.player2.getName();
            }
        }

        /*public int[] calculatePoints()
        {
            int[] points = new int[2];
            int multiplier = 1;

            int p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0;

            //player 1
                //pila azul
            if (game.board.bluepile1.Count > 0)
            {
                p1 -= 20;
                foreach (Card card in game.board.bluepile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p1 += card.getId();
                    }
                }
              
                p1 *= multiplier;
                if (game.board.bluepile1.Count()>7)
                {
                    p1+=20;
                }
            }
                //pila gris
            if (game.board.graypile1.Count > 0)
            {
                p2 -= 20;
                foreach (Card card in game.board.graypile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p2 += card.getId();
                    }                  
                }                
                p2 *= multiplier;
                if (game.board.graypile1.Count() > 7)
                {
                    p2 += 20;
                }
            }
                //pila verde
            if (game.board.greenpile1.Count > 0)
            {
                p3 -= 20;
                foreach (Card card in game.board.greenpile1)
                {                    
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p3 += card.getId();
                    }                    
                }
                p3 *= multiplier;
                if (game.board.greenpile1.Count() > 7)
                {
                    p3 += 20;
                }                
            }
                //pila roja
            if (game.board.redpile1.Count > 0)
            {
                p4 -= 20;
                foreach (Card card in game.board.redpile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p4 += card.getId();
                    }
                }
                p4 *= multiplier;
                if (game.board.redpile1.Count() > 7)
                {
                    p4 += 20;
                }                
            }
                //pila amarilla
            if (game.board.yellowpile1.Count > 0)
            {
                p5 -= 20;
                foreach (Card card in game.board.yellowpile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p5 += card.getId();
                    }                   
                }
                p5 *= multiplier;
                if (game.board.yellowpile1.Count() > 7)
                {
                    p5 += 20;
                }                
            }
                //calculando puntos del primer jugador
            points[0] = p1 + p2 + p3 + p4 + p5;

            p1 = 0; p2 = 0; p3 = 0; p4 = 0; p5 = 0;

            //player 2
                //pila azul
            if (game.board.bluepile2.Count > 0)
            {
                p1 -= 20;
                foreach (Card card in game.board.bluepile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p1 += card.getId();
                    }                   
                }
                p1 *= multiplier;
                if (game.board.bluepile2.Count() > 7)
                {
                    p1 += 20;
                }               
            }
                //pila gris
            if (game.board.graypile2.Count > 0)
            {
                p2 -= 20;
                foreach (Card card in game.board.graypile2)
                {                   
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p2 += card.getId();
                    }                    
                }
                p2 *= multiplier;
                if (game.board.graypile2.Count() > 7)
                {
                    p2 += 20;
                }                
            }
                //pila verde
            if (game.board.greenpile2.Count > 0)
            {
                p3 -= 20;
                foreach (Card card in game.board.greenpile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p3 += card.getId();
                    }                   
                }
                p3 *= multiplier;
                if (game.board.greenpile2.Count() > 7)
                {
                    p3 += 20;
                }               
            }
                //pila roja
            if (game.board.redpile2.Count > 0)
            {
                p4 -= 20;
                foreach (Card card in game.board.redpile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p4 += card.getId();
                    }                    
                }
                p4 *= multiplier;
                if (game.board.redpile2.Count() > 7)
                {
                    p4 += 20;
                }               
            }
                //pila amarilla
            if (game.board.yellowpile2.Count > 0)
            {
                p5 -= 20;
                foreach (Card card in game.board.yellowpile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p5 += card.getId();
                    }
                    
                }
                p5 *= multiplier;
                if (game.board.yellowpile2.Count() > 7)
                {
                    p5 += 20;
                }                
            }
                //calculando puntos del segundo jugador 
            points[1] = p1 + p2 + p3 + p4 + p5;

            return points;
        }*/

        public int[] calculatePoints()
        {
            int[] points = new int[2];
            int multiplier = 1;

            int p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0;

            //player 1
            if (game.board.bluepile1.Count > 0)
            {
                foreach (Card card in game.board.bluepile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p1 += card.getId();
                    }
                }
                if (p1 > 20)
                {
                    p1 += -20;
                    p1 *= multiplier;
                }
                else if (p1 == 20)
                {
                    p1 = 0;
                }
                else
                {
                    p1 = 20 - p1;
                    p1 = -(p1 * multiplier);
                }
            }
            if (game.board.graypile1.Count > 0)
            {
                multiplier = 1;
                foreach (Card card in game.board.graypile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p2 += card.getId();
                    }
                }
                if (p2 > 20)
                {
                    p2 += -20;
                    p2 *= multiplier;
                }
                else if (p2 == 20)
                {
                    p2 = 0;
                }
                else
                {
                    p2 = 20 - p2;
                    p2 = -(p2 * multiplier);
                }
            }
            if (game.board.greenpile1.Count > 0)
            {
                multiplier = 1;
                foreach (Card card in game.board.greenpile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p3 += card.getId();
                    }
                }
                if (p3 > 20)
                {
                    p3 += -20;
                    p3 *= multiplier;
                }
                else if (p3 == 20)
                {
                    p3 = 0;
                }
                else
                {
                    p3 = 20 - p3;
                    p3 = -(p3 * multiplier);
                }
            }
            if (game.board.redpile1.Count > 0)
            {
                multiplier = 1;
                foreach (Card card in game.board.redpile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p4 += card.getId();
                    }
                }
                if (p4 > 20)
                {
                    p4 += -20;
                    p4 *= multiplier;
                }
                else if (p4 == 20)
                {
                    p4 = 0;
                }
                else
                {
                    p4 = 20 - p4;
                    p4 = -(p4 * multiplier);
                }
            }
            if (game.board.yellowpile1.Count > 0)
            {
                multiplier = 1;
                foreach (Card card in game.board.yellowpile1)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p5 += card.getId();
                    }
                }
                if (p5 > 20)
                {
                    p5 += -20;
                    p5 *= multiplier;
                }
                else if (p5 == 20)
                {
                    p5 = 0;
                }
                else
                {
                    p5 = 20 - p5;
                    p5 = -(p5 * multiplier);
                }
            }
            points[0] = p1 + p2 + p3 + p4 + p5;
            p1 = 0; p2 = 0; p3 = 0; p4 = 0; p5 = 0;
            multiplier = 1;

            //player 2
            if (game.board.bluepile2.Count > 0)
            {
                foreach (Card card in game.board.bluepile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p1 += card.getId();
                    }
                }
                if (p1 > 20)
                {
                    p1 += -20;
                    p1 *= multiplier;
                }
                else if (p1 == 20)
                {
                    p1 = 0;
                }
                else
                {
                    p1 = 20 - p1;
                    p1 = -(p1 * multiplier);
                }
            }
            if (game.board.graypile2.Count > 0)
            {
                multiplier = 1;
                foreach (Card card in game.board.graypile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p2 += card.getId();
                    }
                }
                if (p2 > 20)
                {
                    p2 += -20;
                    p2 *= multiplier;
                }
                else if (p2 == 20)
                {
                    p2 = 0;
                }
                else
                {
                    p2 = 20 - p2;
                    p2 = -(p2 * multiplier);
                }
            }
            if (game.board.greenpile2.Count > 0)
            {
                multiplier = 1;
                foreach (Card card in game.board.greenpile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p3 += card.getId();
                    }
                }
                if (p3 > 20)
                {
                    p3 += -20;
                    p3 *= multiplier;
                }
                else if (p3 == 20)
                {
                    p3 = 0;
                }
                else
                {
                    p3 = 20 - p3;
                    p3 = -(p3 * multiplier);
                }
            }
            if (game.board.redpile2.Count > 0)
            {
                multiplier = 1;
                foreach (Card card in game.board.redpile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p4 += card.getId();
                    }
                }
                if (p4 > 20)
                {
                    p4 += -20;
                    p4 *= multiplier;
                }
                else if (p4 == 20)
                {
                    p4 = 0;
                }
                else
                {
                    p4 = 20 - p4;
                    p4 = -(p4 * multiplier);
                }
            }
            if (game.board.yellowpile2.Count > 0)
            {
                multiplier = 1;
                foreach (Card card in game.board.yellowpile2)
                {
                    if (card.getId() == 1)
                    {
                        multiplier++;
                    }
                    else
                    {
                        p5 += card.getId();
                    }
                }
                if (p5 > 20)
                {
                    p5 += -20;
                    p5 *= multiplier;
                }
                else if (p5 == 20)
                {
                    p5 = 0;
                }
                else
                {
                    p5 = 20 - p5;
                    p5 = -(p5 * multiplier);
                }
            }
            points[1] = p1 + p2 + p3 + p4 + p5;

            return points;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            paneltrash.Visible = false;
        }
    }
}
