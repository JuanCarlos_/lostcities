﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace LostCities
{
    public class Card : IComparer<Card>
    {
        private int id;
        private Colors color;
        private int colorindex;

        public Card(int id, Colors color)
        {
            this.id = id;
            this.color = color;
        }

        public int getId()
        {
            return id;
        }

        public Colors getColor()
        {
            return color;
        }

        public int getColorIndex(Colors color)
        {
            switch (color)
            {
                case Colors.Blue:
                    return 0;
                case Colors.Gray:
                    return 1;
                case Colors.Green:
                    return 2;
                case Colors.Red:
                    return 3;
                case Colors.Yellow:
                    return 4;
            }
            return -1;
        }

        public override string ToString()
        {
            return string.Format("id: {0}, color: {1}", id, color);
        }

        public int Compare(Card x, Card y)
        {
            //este método se llama con cada elemento y hace que lo compare con el siguiente
            //acá poné tu criterio de comparación
            //tenés que devolver 1 si el primer elemento es mayor -1 si es menor y 0 si es igual
            if ((x.getId() > y.getId()) && (x.getColorIndex(x.getColor()) > y.getColorIndex(y.getColor()))) return 1;

            if ((x.getId() < y.getId()) && (x.getColorIndex(x.getColor()) < y.getColorIndex(y.getColor()))) return -1;

            return 0;

        }
    }
}
