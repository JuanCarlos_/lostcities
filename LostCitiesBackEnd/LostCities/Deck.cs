﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LostCities
{
    public class Deck
    {
        private Stack<Card> deck;

        public Deck()
        {
            List<Card> list = new List<Card>();

            // create investment cards
            foreach (Colors color in Enum.GetValues(typeof(Colors)))
            {
                for (int i = 0; i <= 2; i++)
                {
                    list.Add(new Card(1, color));
                }
            }

            // create expedition cards

            foreach (Colors color in Enum.GetValues(typeof(Colors)))
            {
                for (int i = 2; i <= 10; i++)
                {
                    list.Add(new Card(i, color));
                }
            }
            list = shuffle(list);
            deck = new Stack<Card>(list);
        }

        // shuffle deck
        private List<Card> shuffle(List<Card> l)
        {
            List<Card> templist = new List<Card>();

            Random rand = new Random();
            while (l.Count > 0)
            {
                int pos = rand.Next(0, l.Count);
                templist.Add(l[pos]);
                l.RemoveAt(pos);
            }
            return templist;
        }

        public Card nextCard()
        {
            return deck.Pop();
        }

        public bool hasCards()
        {
            return deck.Count > 1;
        }

        public int leftCards()
        {
            return deck.Count;
        }
    }
}
