﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LostCities
{
    public class Board
    {
        public List<Card> yellowpile1;
        public List<Card> bluepile1;
        public List<Card> graypile1;
        public List<Card> greenpile1;
        public List<Card> redpile1;

        public List<Card> yellowpile2;
        public List<Card> bluepile2;
        public List<Card> graypile2;
        public List<Card> greenpile2;
        public List<Card> redpile2;

        public Stack<Card> discardedyellowpile;
        public Stack<Card> discardedbluepile;
        public Stack<Card> discardedgraypile;
        public Stack<Card> discardedgreenpile;
        public Stack<Card> discardedredpile;

        public Deck deck;

        public Board()
        {
            deck = new Deck();
            bluepile1 = new List<Card>();
            bluepile2 = new List<Card>();
            graypile1 = new List<Card>();
            graypile2 = new List<Card>();
            greenpile1 = new List<Card>();
            greenpile2 = new List<Card>();
            redpile1 = new List<Card>();
            redpile2 = new List<Card>();
            yellowpile1 = new List<Card>();
            yellowpile2 = new List<Card>();

            discardedyellowpile = new Stack<Card>();
            discardedbluepile = new Stack<Card>();
            discardedgraypile = new Stack<Card>();
            discardedgreenpile = new Stack<Card>();
            discardedredpile = new Stack<Card>();

        }

        public void setCard(Player player, Card card)
        {
            switch (card.getColor())
            {
                case Colors.Blue:
                    if (player.getId() == 1)
                    {
                        bluepile1.Add(card);
                        break;
                    }
                    if (player.getId() == 2)
                    {
                        bluepile2.Add(card);
                    }
                    break;
                case Colors.Gray:
                    if(player.getId() == 1){
                        graypile1.Add(card);
                        break;
                    }
                    if (player.getId() == 2)
                    {
                        graypile2.Add(card);
                    }
                    break;
                case Colors.Green:
                    if (player.getId() == 1)
                    {
                        greenpile1.Add(card);
                        break;
                    }
                    if (player.getId() == 2){
                        greenpile2.Add(card);
                    }
                    break;
                case Colors.Red:
                    if (player.getId() == 1)
                    {
                        redpile1.Add(card);
                        break;
                    }
                    if (player.getId() == 2)
                    {
                        redpile2.Add(card);
                    }
                    break;
                case Colors.Yellow:
                    if (player.getId() == 1)
                    {
                        yellowpile1.Add(card);
                        break;
                    }
                    if (player.getId() == 2)
                    {
                        yellowpile2.Add(card);
                    }
                    break;
            }
        }

        public Card getCardFromDiscardedPile(Colors color)
        {
            Card card = null;
            switch (color)
            {
                case Colors.Blue:
                    if (discardedbluepile != null)
                    {
                        return discardedbluepile.Pop();
                    }
                    break;
                case Colors.Gray:
                    if (discardedgraypile != null)
                    {
                        return discardedgraypile.Pop();
                    }
                    break;
                case Colors.Green:
                    if (discardedgreenpile != null)
                    {
                        return discardedgreenpile.Pop();
                    }
                    break;
                case Colors.Red:
                    if (discardedredpile != null)
                    {
                        return discardedredpile.Pop();
                    }
                    break;
                case Colors.Yellow:
                    if (discardedyellowpile != null)
                    {
                        return discardedyellowpile.Pop();
                    }
                    break;
            }
            return card;
        }

        public List<Card> getPile(Player player, Colors color)
        {
            switch (color)
            {
                case Colors.Blue:
                    if (player.getId() == 1)
                    {
                        return bluepile1;
                    }
                    if (player.getId() == 2)
                    {
                        return bluepile2;
                    }
                    break;
                case Colors.Gray:
                    if (player.getId() == 1)
                    {
                        return graypile1;
                    }
                    if (player.getId() == 2)
                    {
                        return graypile2;
                    }
                    break;
                case Colors.Green:
                    if (player.getId() == 1)
                    {
                        return greenpile1;
                    }
                    if (player.getId() == 2)
                    {
                        return greenpile2;
                    }
                    break;
                case Colors.Red:
                    if (player.getId() == 1)
                    {
                        return redpile1;
                    }
                    if (player.getId() == 2)
                    {
                        return redpile2;
                    }
                    break;
                case Colors.Yellow:
                    if (player.getId() == 1)
                    {
                        return redpile1;
                    }
                    if (player.getId() == 2)
                    {
                        return redpile2;
                    }
                    break;
            }
            return null;
        }
    }
}