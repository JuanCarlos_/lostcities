﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LostCities
{
    public class Player
    {
        private int id;
        private string name;
        private List<Card> hand;
        private int points;

        public void setId(int id)
        {
            this.id = id;
        }

        public int getId()
        {
            return id;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public string getName()
        {
            return name;
        }

        public void setHand(List<Card> hand)
        {
            this.hand = hand;
        }

        public List<Card> getHand()
        {
            return hand;
        }

        public void addCard(Card card)
        {
            if (hand.Count < 8)
            {
                this.hand.Add(card);
            }
        }

        public void setPoints(int points)
        {
            this.points += points;
        }

        public int getPoints()
        {
            return points;
        }
    }
}
