﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LostCities
{
    public class Game
    {
        public Board board;
        private Card currentcard;

        public Player player1;
        public Player player2;

        public Game()
        {
            board = new Board();
        }

        public void addPlayer(string name)
        {
            if (player1 == null)
            {
                player1 = new Player();
                player1.setId(1);
                player1.setName(name);
            }
            else
            {
                player2 = new Player();
                player2.setId(2);
                player2.setName(name);
            }
        }

        public void setHand(Player player)
        {
            List<Card> cardslist;
            switch (player.getId())
            {
                case 1:
                        cardslist = new List<Card>();
                        for (int i = 0; i <= 7; i++)
                        {
                            
                            cardslist.Add(board.deck.nextCard());
                        }
                        player1.setHand(cardslist);
                    break;
                case 2:
                        cardslist = new List<Card>();
                        for (int i = 0; i <= 7; i++)
                        {
                            cardslist.Add(board.deck.nextCard());
                        }
                        player2.setHand(cardslist);
                    
                    break;
            }
        }

        public bool setCard(Player player, Card card)
        {
            switch (card.getColor())
            {
                case Colors.Blue:
                    if(player.getId() == 1){
                        if(Rules.canSetCard(board.bluepile1, card)){
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    if (player.getId() == 2)
                    {
                        if (Rules.canSetCard(board.bluepile2, card))
                        {
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    return false;
                case Colors.Gray:
                    if(player.getId() == 1){
                        if(Rules.canSetCard(board.graypile1, card)){
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    if (player.getId() == 2)
                    {
                        if (Rules.canSetCard(board.graypile2, card))
                        {
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    return false;
                case Colors.Green:
                    if (player.getId() == 1)
                    {
                        if(Rules.canSetCard(board.greenpile1, card)){
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    if (player.getId() == 2)
                    {
                        if (Rules.canSetCard(board.greenpile2, card))
                        {
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    return false;
                case Colors.Red:
                    if (player.getId() == 1)
                    {
                        if (Rules.canSetCard(board.redpile1, card))
                        {
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    if (player.getId() == 2)
                    {
                        if (Rules.canSetCard(board.redpile2, card))
                        {
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    return false;
                case Colors.Yellow:
                    if (player.getId() == 1)
                    {
                        if(Rules.canSetCard(board.yellowpile1, card)){
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    if (player.getId() == 2)
                    {
                        if (Rules.canSetCard(board.yellowpile2, card))
                        {
                            board.setCard(player, card);
                            return true;
                        }
                    }
                    return false;
            }
            return false;
        }

        public Card nextCard(){
            if (board.deck.hasCards())
            {
                currentcard = board.deck.nextCard();
                return currentcard;
            }
            return null;
        }

        // take card when you just set one
        public void addCard(Player player, Card card){
            switch (player.getId())
            {
                case 1:
                    player1.addCard(card);
                    break;
                case 2:
                    player2.addCard(card);
                    break;
            }
        }

        public List<Card> getPile(Player player, Colors color)
        {
            return board.getPile(player, color);
        }

        public Card getCurrentCard()
        {
            return currentcard;
        }

        public void discardCard(Player player, int index)
        {

            switch (player.getHand()[index].getColor())
            {
                case Colors.Blue:
                    if (player.getId() == 1)
                    {
                        board.discardedbluepile.Push(player.getHand()[index]);
                        player1.getHand().RemoveAt(index);
                    }
                    else
                    {
                        board.discardedbluepile.Push(player.getHand()[index]);
                        player2.getHand().RemoveAt(index);
                    }
                    break;
                case Colors.Gray:
                    if (player.getId() == 1)
                    {
                        board.discardedgraypile.Push(player.getHand()[index]);
                        player1.getHand().RemoveAt(index);
                    }
                    else
                    {
                        board.discardedgraypile.Push(player.getHand()[index]);
                        player2.getHand().RemoveAt(index);
                    }
                    break;
                case Colors.Green:
                    if (player.getId() == 1)
                    {
                        board.discardedgreenpile.Push(player.getHand()[index]);
                        player1.getHand().RemoveAt(index);
                    }
                    else
                    {
                        board.discardedgreenpile.Push(player.getHand()[index]);
                        player2.getHand().RemoveAt(index);
                    }
                    break;
                case Colors.Red:
                    if (player.getId() == 1)
                    {
                        board.discardedredpile.Push(player.getHand()[index]);
                        player1.getHand().RemoveAt(index);
                    }
                    else
                    {
                        board.discardedredpile.Push(player.getHand()[index]);
                        player2.getHand().RemoveAt(index);
                    }
                    break;
                case Colors.Yellow:
                    if (player.getId() == 1)
                    {
                        board.discardedyellowpile.Push(player.getHand()[index]);
                        player1.getHand().RemoveAt(index);
                    }
                    else
                    {
                        board.discardedyellowpile.Push(player.getHand()[index]);
                        player2.getHand().RemoveAt(index);
                    }
                    break;
            }
        }

        public int getLeftCards(){
            return board.deck.leftCards();
        }
    }
}
