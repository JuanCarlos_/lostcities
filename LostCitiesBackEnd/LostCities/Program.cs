﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LostCities
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            game.addPlayer("Juan Carlos");
            game.setHand(game.player1);
            List<Card> lista = game.player1.getHand();
            game.addPlayer("Edgar");
            game.setHand(game.player2);
            

            while (game.nextCard() != null)
            {
                foreach (Card card in game.player1.getHand())
                {
                    Console.WriteLine(card);
                }
                Console.WriteLine("Carta a poner: ");
                int x = Convert.ToInt16(Console.ReadLine());
                Card c = new Card(game.player1.getHand()[x].getId(), game.player1.getHand()[x].getColor());
                if (game.setCard(game.player1, c))
                {
                    game.discardCard(game.player1, x);
                    game.addCard(game.player1, game.getCurrentCard());
                }

                Console.WriteLine("~ ~ ~ ~ ~ ~ ~ ~ ~");
                foreach (Card card in game.player2.getHand())
                {
                    Console.WriteLine(card);
                }                
                Console.WriteLine("Carta a poner: ");
                x = Convert.ToInt16(Console.ReadLine());
                c = new Card(game.player2.getHand()[x].getId(), game.player2.getHand()[x].getColor());
                if (game.setCard(game.player2, c))
                {
                    game.discardCard(game.player2, x);
                    game.addCard(game.player2, game.getCurrentCard());
                }

                Console.ReadLine();
            }
        }
    }
}
