﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LostCities
{
    public class Rules
    {
        // you can set card if last card in pile is < than card in your hand only
        public static bool canSetCard(List<Card> pile, Card card)
        {
            if (pile.Count == 0)
            {
                return true;
            }
            return pile[pile.Count - 1].getId() <= card.getId();
        }

        public static bool discardCard(List<Card> pile)
        {
            if (pile.Count == 0)
            {
                return true;
            }
            return pile.Count == 8;
        }

        public static bool drawCard(List<Card> pile)
        {
            if (pile.Count == 0)
            {
                return true;
            }
            return pile.Count < 8;
        }
    }
}
