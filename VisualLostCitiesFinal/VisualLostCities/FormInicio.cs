﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VisualLostCities
{
    public partial class FormInicio : Form
    {
        public FormInicio()
        {
            InitializeComponent();
        }

        private void buttonComenzar_Click(object sender, EventArgs e)
        {
            string[] names = new string[2];
            int[] ages = new int[2];
            names[0] = textBoxPlayer1.Text;
            names[1] = textBoxPlayer2.Text;
            ages[0] = int.Parse(textBoxEdad1.Text);
            ages[1] = int.Parse(textBoxEdad2.Text);

            FormLostCities flc = new FormLostCities(names, ages);
            flc.Show();
            this.Hide();
        }
    }
}
