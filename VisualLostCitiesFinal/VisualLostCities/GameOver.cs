﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VisualLostCities
{
    public partial class GameOver : Form
    {
        int[] points;
        string[] names;

        public GameOver(int[] points, string[] names)
        {
            InitializeComponent();
            this.points = points;
            this.names = names;
        }

        private void GameOver_Load(object sender, EventArgs e)
        {
            labelPlayer1.Text = "Jugador: " + names[0] + ", " + points[0];
            labelPlayer2.Text = "Jugador " + names[1] + ", " + points[1];
            if (points[0] > points[1])
            {
                labelGanador.Text = names[0];
            }
            else if (points[1] > points[0])
            {
                labelGanador.Text = names[1];
            }
            else
            {
                labelGanador.Text = "Empate!!";
            }
        }
    }
}
